    .set noreorder
    .data

print:
        #void print(int a)
        #{
                ori $v0, $0, 20 # v0 = 20
                syscall 
                jr $ra
                nop
        #}

sum3:
        #int sum3(int a, int b, int c)
        #{
                add $t0, $a0, $a1 #t0 = a0 + a1
                add $v0, $t0, $a2 #v0 = t0 + a2
                jr $ra
                nop
        #}

polynomial:
        #int polynomial(int a, int b, int c, int d, int e)
        #{
            addi $sp, $sp, -4 #sp = sp - 12
            sw $ra, 0($sp) #push ra onto stack
                
            #int x;
            #use s0

            # int y;
            #use s1

            # int z;
            #use s2

            # x = a << b;
            sll $s0, $a0, $a1 # s0 = a0 << a1

            # y = c << d;
            sll $s1, $a2, $a3 # s1 = a2 << a3

            # z = sum3(x, y, e);
            ori $a0, $s0, 0 #a0 = s0
            ori $a1, $s1, 0 #a1 = s1
            ori $a2, $0, 0 #a2 = 0
            lw $a2, 4($sp) # a2 = *(sp+12)
            jal sum3
            nop
            ori $s2, $v0, 0 #s2 = v0


            # print(x);
            ori $a0, $s0, 0 #a0 = s0
            jal print
            nop 

            # print(y);
            ori $a0, $s1, 0 #a0 = s1
            jal print
            nop 

            # print(z);
            ori $a0, $s2, 0 #a0 = 2
            jal print
            nop 

            # return z;
            ori $v0, $s2, 6 #v0 = s2

            lw $ra, 0($sp) #pop ra from stack
            addi $sp, $sp, 4 #sp = sp+4
            jr $ra
            nop
        #}

    .text
    .globl main
    .ent main
main:

#int main()
#{
        add $sp, $sp, -16 #sp = sp - 16
        sw $ra, 12($sp) #push ra onto stack
        sw $s0, 8($sp) #push s0 onto stack
        sw $s1, 4($sp) #push s1 onto stack

        #int a = 2;
        ori $s0, $0, 2 #s0 = 2

        #int f = polynomial(a, 3, 4, 5, 6);
        add $a0, $0, $s0 #a0 = s0
        addi $a1, $0, 3 #a1 = 3
        addi $a2, $0, 4 #a2 = 4
        addi $a3, $0, 5 #a3 = 5
        addi $t0, $0, 6 #t0 = 6
        sw $t0, 0($sp) #*(sp) = t0
        jal polynomial
        nop

        ori $s1, $v0, 0 #s1 = v0

        #print(a);
        ori $a0, $s0, 0 #a0 = s0
        jal print
        nop

        #print(f);
        ori $a0, $s1, 0 #a0 = s0
        jal print
        nop

        lw $ra, 12($sp) #pop ra off stack
        lw $s0, 8($sp) #pop s0 off stack
        lw $s1, 4($sp) #pop s1 off stack
        add $sp, $sp, 16 #sp = sp + 16
        jr $ra
        nop
#} 

    .end main
