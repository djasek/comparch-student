    .set noreorder
    .data
        # char A[] = { 1, 25, 7, 9, -1 };
        A:  .byte   1, 25, 7, 9, -1 #initialize global array
       
    .text
    .globl main
    .ent main
main:
    #int main() {
       
        #int i;
        #use s0

        #int current;
        #use s2

        #int max;
        #use s3

        #i = 0;
        addi $s0, $0, 0 #s0 = 0

        #current = A[0];
        la $s1, A #s1 = &A
        lb $s2, 0($s1) #s2 = A[0]

        #max = 0;
        addi $s3, $0, 0 #s3 = 0

start_while:
        #while (current > 0)
        slt $t0, $0, $s2
        beq $t0, $0, end_while #if(t0) goto end_while

            #if (current > max)
            slt $t0, $s2, $s3 #t0 = s3 < s2
            bne $t0, $0, end_if #if(!t0) goto end_if

            or $a0, $0, $s3 #a0 = s3
 
            #max = current;
            ori $s3, $s2, 0 #s3 = s2

end_if:

            #i = i + 1;
            addi $s0, $s0, 1 #s1 = s1 + 1

            #current = A[i];
            add $t0, $s1, $s0 #t0 = s1 + s0
            lb $s2, 0($t0) #s2 = *(t0)
            #or $s2, $s1($s0) #s2 = A[s1]

            j start_while
end_while:
    
        #PRINT_HEX_DEC(max);   
        ori $a0, $s3, 0 #a0 = s3
        ori $v0, $0, 20 #v0 = 20
        syscall

        #EXIT;
        ori $v0, $0, 10     # exit
        syscall
    #}
    .end main
