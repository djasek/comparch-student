    .set noreorder
    .data
        #typedef struct record {
        #int field0;
        #int field1;
        #} record;

    .text
    .globl main
    .ent main
main:

        #int main()
        #{
                #record *r = (record *) MALLOC(sizeof(record));
                ori $a0, $0, 8 #a0 = 8
                ori $v0, $0, 9 # v0 = 9 (malloc)
                syscall
                ori $s0, $v0, 0 #s0 = &a0

                #r->field0 = 100;
                addi $t0, $0, 100 #t0 = 100
                sw $t0, 0($s0) #M[s0] = t0
                #r->field1 = -1;
                addi $t0, $0, -1 #t0 = -1
                sw $t0, 4($s0) #M[s0 + 1] = t0

                #EXIT;
                ori $v0, $zero, 10     # exit
                syscall
        #}
    .end main
