    .set noreorder
    .data
        
        #int A[8]
        B: .space 32 #declare 32 bytes of empty space
    .text
    .globl main
    .ent main
main:

        #int main()
        #{
                #int i;
                #using reg s1    

                #A[0] = 0;
                la $s3, B #load the address of B into s3
                sw $0, 0($s3) #A+0 is 0         
    
                #A[1] = 1;
                addi $t0, $0, 1 #t0 = 1
                sw $t0, 4($s3) #A+4 is 1

                #for (i = 2; i < 8; i++) {
                addi $s1, $0, 8 #i=8
loop_cond:
                slti $t0, $s1, 32 #t0 = (i < 32)
                beq $t0, $0, sequel #if !t0, go to sequel
                nop
                
                        #A[i] = A[i-1] + A[i-2];
                        add $t1, $s3, $s1 #t1 = address of B + i
                        lw $t2, -4($t1) #t2 = A[i-1]
                        lw $t3, -8($t1) #t3 = A[i-2]
                        add $t4, $t2, $t3 #s2 = $t2 + $t3
                        sw $t4, 0($t1) #A[i] = t4

                        #PRINT_HEX_DEC(A[i]);
                        or $a0, $0, $t4 # a0 = t4
                        ori $v0, $0, 20 # v0 = 20
                        syscall
                #}

                addi $s1, $s1, 4 #i = i+4
                j loop_cond #goto loop_cond
                nop

sequel:
                #EXIT;
                ori $v0, $zero, 10     # exit
                syscall
        #} 
    .end main
