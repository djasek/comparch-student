    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:
        #int score = 84;
        addi $s0, $0, 84 # s0 = 84
        #int grade;

        #if (score >= 90)
        slti $t0, $s0, 90 # t0 = s0 < 90
        bne $t0, $zero, sequel_1 # if(!t0) goto sequel_1
        nop

        #grade = 4;
        addi $s1, $0, 4 # s1 = 4
        #else if (score >= 80)
        j end_else_if #goto end_else_if
        nop
sequel_1:
        slti $t0, $s0, 80 #t0 = s0 < 80
        bne $t0, $zero, sequel_2 # if(!t0) goto sequel_2
        nop

        #grade = 3;
        addi $s1, $0, 3 # s1 = 3
        j end_else_if #goto end_else_if
        nop
        #else if (score >= 70)
sequel_2:
        slti $t0, $s0, 70 # t0 = s0 < 70     
        bne $t0, $zero, else # if(!t0) goto else
        nop

        #grade = 2;
        addi $s1, $0, 2 #s1 = 2

        j end_else_if #goto end_else_if
        nop
else:
        #else
        #grade = 0;
        addi $s1, $0, 0 # s1 = 0
        j end_else_if #goto end_else_if
        nop
end_else_if:
        #PRINT_HEX_DEC(grade);
        or $a0, $0, $s1 # a0 = s1
        ori $v0, $0, 20 # v0 = 20
        syscall

        #EXIT;
        ori $v0, $0, 10     # exit
        syscall
    .end main
