    .set noreorder
    .data
        #typedef struct elt {
            #int value;
            #struct elt *next;
        #} elt;

    .text
    .globl main
    .ent main
main:
        #int main()
        #{
                #elt *head;
                #elt *newelt;

                #newelt = (elt *) MALLOC(sizeof (elt));
                ori $a0, $0, 16 #a0 = 16
                ori $v0, $0, 9 # v0 = 9 (malloc)
                syscall
                ori $s0, $v0, 0 #s0 = v0

                #newelt->value = 1;
                addi $t0, $0, 1 #t0 = 1
                sw $t0, 0($s0) #M[s0] = t0

                #newelt->next = 0;
                addi $t0, $0, 0 #t0 = 0
                sw $t0, 4($s0) #M[s0+4] = t0 

                #head = newelt;
                ori $s1, $s0, 0 #s1 = s0

                #newelt = (elt *) MALLOC(sizeof (elt));
                ori $a0, $0, 16 #a0 = 16
                ori $v0, $0, 9 # v0 = 9 (malloc)
                syscall
                ori $s0, $v0, 0 #s0 = v0

                #newelt->value = 2;
                addi $t0, $0, 2 #t0 = 2
                sw $t0, 0($s0) #M[s0] = t0

                #newelt->next = head;
                ori $t0, $s1, 0 #t0 = s1
                sw $t0, 4($s0) #M[s0+4] = t0

                #head = newelt;
                ori $s1, $s0, 0 #s1 = s0

                #PRINT_HEX_DEC(head->value);
                lw $a0, 0($s1) # a0 = M[s1]
                ori $v0, $0, 20 # v0 = 20
                syscall

                #PRINT_HEX_DEC(head->next->value);
                lw $t0, 4($s1) #t0 = M[s1+4]
                lw $a0, 0($t0) # a0 = M[s1]
                ori $v0, $0, 20 # v0 = 20
                syscall
    
                #EXIT;
                ori $v0, $zero, 10     # exit
                syscall
        #}

.end main
