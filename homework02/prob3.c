#include <stdio.h>

unsigned func1(unsigned a) {
    a = a >> 4;
    a = a & 255;
    return a;
}

int main() {
    unsigned a = 43981;
    unsigned b = func1(a);
    printf("%u\n", b);
    printf("%08x\n", b);
}


