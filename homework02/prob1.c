#include <stdio.h>
#include <stdlib.h>

int A;
int B = 0;
int array1[1024];

void func1() {
    int func1_0, func1_1, func1_2;
    printf("Address of func1_0: %018p\n", &func1_0);
    printf("Address of func1_1: %018p\n", &func1_1);
    printf("Address of func1_2: %018p\n", &func1_2);

}

void func2(int func2_0, int func2_1, int func2_2) {
    printf("Address of func2_0: %018p\n", &func2_0);
    printf("Address of func2_1: %018p\n", &func2_1);
    printf("Address of func2_2: %018p\n", &func2_2);

}

int main() {
    char C;
    int D = 0;
    char array2[1024];
    long array3[1024];
    short array4[1024];
    func1();
    int E, F, G;
    func2(E, F, G);
    char *allocated = (char *)malloc(1024);

    printf("Address of A: %018p\n", &A);
    printf("Address of B: %018p\n", &B);
    printf("Address of array1: %018p\n", &array1);
    printf("Size of array1: %d\n", sizeof(array1));
    printf("Address of C: %018p\n", &C);
    printf("Address of D: %018p\n", &D);
    printf("Address of array2: %018p\n", &array2);
    printf("Size of array2: %d\n", sizeof(array2));
    printf("Size of array3: %d\n", sizeof(array3));
    printf("Size of array4: %d\n", sizeof(array4));
    printf("Address of allocated: %018p\n", &allocated);
    printf("Address of main: %018p\n", &main);
    printf("Address of func1: %018p\n", &func1);
    printf("Address of func2: %018p\n", &func2);
}
