#include <stdio.h> 

void sumArray(int * arr, int * sum, int n); 

int main() {
    int sum = 0;
    int arr[8];
    arr[0] = 17; arr[1] = -3; arr[2] = 8; arr[3] = 49; arr[4] = -5; arr[5] = 56; arr[6] = 100; arr[7] = -22; 

    sumArray(arr, &sum, 4);
    printf("%d\n", sum);

    sumArray(arr, &sum, 6);
    printf("%d\n", sum);

}

void sumArray(int * arr, int * sum, int n) {
    int i;
    for(i=0; i<n; i++) {
        *sum+=arr[i];
    }
}
