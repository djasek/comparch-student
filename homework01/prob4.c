#include <stdio.h>

int signext(int n, unsigned s);

int main() {

    printf("%08x\n", signext(0x8f, 8));
    printf("%08x\n", signext(0x18f, 12));
    printf("%08x\n", signext(0x8f, 9));
    printf("%08x\n", signext(0xabcd, 16));

}

int signext(int n, unsigned s) {


    if((n >> s-1) > 0) {

        unsigned numToOr = 0x0000000f;
        int i;

        for(i=1; i<8; i++) {
            numToOr = numToOr << 4;
            if(i*4 >= s) {
                //printf("%d\n", i);
                n = n | numToOr;
            }
        }
    }

    return n;
}
