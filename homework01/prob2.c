// collaborated with Eric Krebs

#include <stdio.h>

void printbin(char n);

int main() {
    printf("\n-128: "); printbin(-128);
    printf("\n-65: "); printbin(-65);
    printf("\n-5: "); printbin(-5);
    printf("\n-1: "); printbin(-1);
    printf("\n0: "); printbin(0);
    printf("\n1: "); printbin(1);
    printf("\n5: "); printbin(5);
    printf("\n65: "); printbin(65);
    printf("\n127: "); printbin(127);

}

void printbin(char n) {

    if(n < 0) {
        n = 128 + n;
        printf("1");
    } else {
        printf("0");
    }

    char divider = 64;

    while(divider > 0) {
        if(n >= divider) {
            printf("1");
            n = n - divider;
        } else {
            printf("0");
        }

        divider = divider/2;
    }
}
