#include <stdio.h>

unsigned fib(unsigned n);

int main() {
    printf("%d\n", fib(5));
}

unsigned fib(unsigned n) {

    printf("address of %d: %018p\n", n, &n);

    if(n == 0) return 0;
    if(n == 1) return 1;

    return fib(n-1) + fib(n-2);
}
