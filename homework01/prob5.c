#include <stdio.h>

int concat(short m, short n);

int main() {

    short m = 0xffff;
    short n = 0xabcd;
    printf("%08x\n", concat(m, n));

    m = 0x0000;
    n = 0x0000;
    printf("%08x\n", concat(m, n));

    m = 0xdead;
    n = 0xbeaf;
    printf("%08x\n", concat(m, n));

    m = 0x1234;
    n = 0x5678;
    printf("%08x\n", concat(m, n));

}

int concat(short m, short n) {

    int num1 = 0x00000000;
    num1 = num1 | m;
    num1 = num1 << 16;

    int num2 = 0x00000000;
    num2 = num2 | n & 0x0000ffff;

    num1 = num1 | num2;

    return num1;
}
