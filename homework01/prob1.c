#include <stdio.h>
#include <stdint.h>

void printhex(int num);
void printunsigned(int num); 
void printsigned(int num);

int main() {
    uint8_t unum1 = -85;
    uint8_t unum2 = 18;
    signed int unum3 = 0xcd;

    printhex(unum1);
    printunsigned(unum1);
    printhex(unum2);
    printsigned(unum2);
    printsigned(unum3);
    printunsigned(unum3);
}

void printhex(int num) {
    printf("%08x\n", num);
}

void printunsigned(int num) {
    printf("%u\n", num);
}

void printsigned(int num) {
    printf("%d\n", num);
}
