#include <stdio.h>
#include <string.h>
#include <stdlib.h>

unsigned bitrange(unsigned inst, unsigned hi, unsigned lo);

int main() {

    unsigned inst = 0xabcd1234;

    printf("%x\n", bitrange(inst, 3, 0));
    printf("%x\n", bitrange(inst, 31, 28));
    printf("%x\n", bitrange(inst, 27, 4));
    printf("%x\n", bitrange(inst, 7, 4));
    printf("%x\n", bitrange(inst, 11, 8));
}

unsigned bitrange(unsigned inst, unsigned hi, unsigned lo) {

    int numToAnd = 0x00000000;
    int transformationNum;
    int i;
    for(i=0; i<32; i+=4) {

        if(i==0) {
            transformationNum = 0x0000000f;
        } else {
            transformationNum = transformationNum << 4;
        }
        if(i >= lo && i < hi) {
            //printf("%08x\n", transformationNum);
            numToAnd = numToAnd | transformationNum;
        }

    }

    return (inst & numToAnd) >> lo;
}
